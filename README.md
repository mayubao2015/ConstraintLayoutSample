# ConstraintLayoutSample

#### 项目介绍
ConstraintLayoutSample

#### ConstraintLayout的使用

##### ConstraintLayout的简单使用
    使用android_constraintxxx_xxx特性来使用约束限制View的方位
    简单举例子：
    左上角
                app:layout_constraintLeft_toLeftOf="parent" <!-- 可以简单理解为左边约束：父布局的左边-->
                app:layout_constraintTop_toTopOf="parent"   <!-- 可以简单理解为顶部约束：父布局的顶部-->
                <!-- 添加这两个约束，刚好不就是左上角了么？-->
    所以同理，右上角，左下角，右下角也是可以用两个约束来实现的


##### ConstraintLayout嵌套ListView RecycleView

    ListView RecycleView的width和height 一定要设置为 match_constraint = 0， 才会正常显示


##### 常见的约束

 - 上下左右四个约束

    app:layout_constraintLeft_xxx
    app:layout_constraintRight_xxx
    app:layout_constraintTop_xxx
    app:layout_constraintBottom_xxx

 - 开始结束两个约束

    app:layout_constraintStart_xxx
    app:layout_constraintEnd_xxx

 - 垂直和水平的偏移量约束、
    app:layout_constraintVertical_bias
    app:layout_constraintHorizontal_bias



> 基本上面由上下左右和开始结束六个约束可以决定控件在ConstraintLayout的布局的位置了




